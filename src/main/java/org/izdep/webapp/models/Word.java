package org.izdep.webapp.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "terms")
public class Word {

  @Id
  @GeneratedValue
  @Column(name = "id")
  private int id;

  String name;
  String type;
  Integer quantity;
  Double percentage;

  @Override
  public boolean equals(Object other){
    if (other == null) return false;
    if (other == this) return true;
    if (!(other instanceof Word))return false;
    Word otherMyClass = (Word)other;
    return this.name.equals(otherMyClass.getName());
  }
}