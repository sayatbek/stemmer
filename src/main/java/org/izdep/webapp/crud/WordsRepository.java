package org.izdep.webapp.crud;

import java.util.List;
import org.izdep.webapp.models.Word;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WordsRepository extends CrudRepository<Word, Long> {
  List<Word> findAll();
  List<Word> findByName(String name);
}
