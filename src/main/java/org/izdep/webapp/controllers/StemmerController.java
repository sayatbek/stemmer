package org.izdep.webapp.controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.izdep.webapp.models.Word;
import org.izdep.webapp.service.StemWordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class StemmerController {

  private static final Logger logger = LogManager.getLogger(StemmerController.class);

  @Autowired
  StemWordService stemWordService;

  @RequestMapping(value = "/stem", method = RequestMethod.GET, produces = "application/json")
  @ApiOperation(value = "Get a word and generate stemmed version of it")
  public ResponseEntity<List<Word>> stemWord(
      @ApiParam(name = "word", value = "word to be stemmed", required = true)
      @RequestParam(name = "word") String word
  ) {
    logger.info("the word came is: " + word);
    return new ResponseEntity<>(stemWordService.getStemmedWord(word), HttpStatus.OK);
  }

  @RequestMapping(value = "/stemlist", method = RequestMethod.POST, produces = "application/json")
  @ApiOperation(value = "Get a list of words and generate stemmed versions of them")
  public ResponseEntity<Set<Word>> stemWordList(
       @ApiParam(name = "words", value = "array of words to be stemmed", required = true)
      @RequestBody String request
  ) {
    List<String> words = new Gson().fromJson(request, new TypeToken<List<String>>(){}.getType());
    return new ResponseEntity(stemWordService.getStemmedWordList(words), HttpStatus.OK);
  }
}
