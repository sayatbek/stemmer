package org.izdep.webapp.service;

import java.util.List;
import java.util.Set;
import org.izdep.webapp.models.Word;

public interface StemWordService {

  List<Word> getStemmedWord(String word);

  Set<String> getStemmedWordList(List<String> words);
}
