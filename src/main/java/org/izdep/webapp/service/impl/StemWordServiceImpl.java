package org.izdep.webapp.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.izdep.webapp.crud.WordsRepository;
import org.izdep.webapp.models.Word;
import org.izdep.webapp.service.StemWordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Service
public class StemWordServiceImpl implements StemWordService {

  @Autowired
  WordsRepository wordsRepository;

  @Override
  public List<Word> getStemmedWord(String word) {
    while(word.length()>2){
      word = word.substring(0, word.length()-1);
      List<Word> foundWord = wordsRepository.findByName(word);
      if(foundWord != null && foundWord.size()>0){
        return foundWord;
      }
    }
    return null;
  }



  @Override
  public Set<String> getStemmedWordList(List<String> words) {
    Set<String> wordSetDb = new HashSet<>();
    for(Word w : wordsRepository.findAll()){
      wordSetDb.add(w.getName());
    }

    Set<String> wordSet = new HashSet<>();
    for(String word : words){
      if(wordSetDb.contains(word)){
        wordSet.add(word);
      }
    }
    return wordSet;
  }
}
