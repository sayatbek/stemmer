#Author

@Sayatbek 
ISTEP Solutions
Dortmund, Germany
2018

#Stemmer Project
This project designed to stem (cut off the endings) the words in Kazakh language.
Minimal Spring Boot based RESTful API, including Swagger (using Springfox) and Docker
(Also see https://spring.io/guides/gs/rest-service/)

#Clone the source code

Go to:

https://bitbucket.org/sayatbek/stemmer

or run this bash command:

```
$ git clone https://sayatbek@bitbucket.org/sayatbek/stemmer.git
```

Requirements
------------

* JDK: 1.8
* Maven version: 3.3.3 (Please double check Maven .m2/settings.xml)
* Tomcat: 8

Build and deploy
----------------

sh build.sh

Browser
-------

http://127.0.0.1:8080/hello-world-webapp-1.0.0-VERSION/

Docker
------

* Build webapp as jar file (see pom.xml)
* Start Docker
* Build image: docker build -t spring-boot-hello-world .
* Show images: docker images
** Remove image: docker rmi -f IMAGE ID
* Run image: docker run -p 8080:8080 spring-boot-hello-world
* Show docker processes: docker ps
* Stop specific docker process: docker stop CONTAINER ID
* Request in browser: http://127.0.0.1:8080/swagger-ui.html

Specification and Testing
-------------------------

http://ec2-35-165-39-175.us-west-2.compute.amazonaws.com:8585/api/stem?word=%D0%B0%D0%BB%D0%BC%D0%B0%D1%82%D1%8B
